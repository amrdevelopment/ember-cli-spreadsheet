import EmberMap from '@ember/map';
import Mixin from '@ember/object/mixin';

export default Mixin.create({
	/**
	 * Each key is the name of the workbook
	 * 
	 * @property workbooks
	 * @private
	 * @default {}
	 * @type {Map}
	 */
	_workbooks: null,

	/**
	 * Overriding Ember.Mixin.init. Setup is done here.
	 *
	 * @method init
	 */
	init() {
		this._super(...arguments);
		this.set('_workbooks', new EmberMap());
	},

	/**
	 * Gets the workbook object based on the name.
	 * See the https://github.com/SheetJS/js-xlsx#working-with-the-workbook for details on
	 * how to work with the objects.
	 *
	 * @method getWorkbook
	 * @param  {String} fileName - the name of the workbook (with or without extension)
	 * @return {Object} workbook
	 */
	getWorkbook(fileName) {
		let workbooks = this.get('_workbooks');
		let file = fileName.split('.')[0];
		return workbooks.get(file);
	}, 

	/**
	 * Adds a workbook to the public and private workbooks vars.
	 *
	 * @method addWorkbook
	 * @param {String} fileName - the name of the workbook/file
	 * @param {Object} workbook - the workbook object
	 */
	addWorkbook(fileName, workbook) {
		let workbooks = this.get('_workbooks');
		let file = fileName.split('.')[0];
		workbooks.set(file, workbook);
	},

	/**
	 * Removes a workbook from the workbooks object.
	 *
	 * @method removeWorkbook
	 * @param  {String} fileName - the name of the workbook (with or without extension)
	 */
	removeWorkbook(fileName) {
		let workbooks = this.get('_workbooks');
		let file = fileName.split('.')[0]
		workbooks.delete(file);
	},

	/**
	 * Clears the underlying map of workbooks.
	 *
	 * @method clear
	 */
	clear() {
		let workbooks = this.get('_workbooks');
		workbooks.clear();
	},

	/**
	 * The forEach() method executes a provided function once per each workbook in the underlying
	 * Map object, in insertion order.
	 *
	 * @method forEach
	 * @param  {Function} callback - Function to execute for each element. Recieves three arguments: 
	 *                             	 the element value, the element key, and the map object being traversed
	 * @param  {Object}   thisArg  - value to use as 'this' when executing the callback
	 * 
	 */
	forEach(callback, thisArg) {
		let workbooks = this.get('_workbooks');
		workbooks.forEach(callback, thisArg);
	},

	/**
	 * Checks if the underlying workbooks map contains the workbook.
	 * @param  {String} fileName - the name of the workbook (with or without extension)
	 * @return {Boolean} returns true if the workbook exists
	 */
	containsFile(fileName) {
		let workbooks = this.get('_workbooks');
		let file = fileName.split('.')[0]
		return workbooks.has(file);
	}
});
