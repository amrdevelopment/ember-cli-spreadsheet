import EmberObject from '@ember/object';
import WorkbooksMixin from 'ember-cli-spreadsheet/mixins/ember-spreadsheet-workbooks';
import { module, test } from 'qunit';

module('Unit | Mixin | ember-spreadsheet-workbooks');

test('it can be instantiated', (assert) => {
  let WorkbooksObject = EmberObject.extend(WorkbooksMixin);
  let subject = WorkbooksObject.create();

  assert.ok(subject);
});

test('it can add a workbook given a filename and extension to the workbook map', (assert) => {
	let WorkbooksObject = EmberObject.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let file = 'test1';
	let ext = '.xlsx';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(file + ext, testWorkbook);

	assert.ok(subject.get('_workbooks').has(file));
});

test('it can add a workbook given a filename with no extension to the workbook map', (assert) => {
	let WorkbooksObject = EmberObject.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let file = 'test1';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(file, testWorkbook);

	assert.ok(subject.get('_workbooks').has(file));
});

test('it can remove a workbook from the map given a filename without extension', (assert) => {
	let WorkbooksObject = EmberObject.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let file = 'test1';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(file, testWorkbook);
	subject.removeWorkbook(file);

	assert.notOk(subject.get('_workbooks').has(file));
});

test('it can remove a workbook from the map given a filename with extension', (assert) => {
	let WorkbooksObject = EmberObject.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let file = 'test1';
	let ext = '.xlsx';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(file, testWorkbook);
	subject.removeWorkbook(file + ext);

	assert.notOk(subject.get('_workbooks').has(file));
});

test('it can get a workbook by name, without extension, from the workbook map', (assert) => {
	let WorkbooksObject = EmberObject.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let file = 'test1';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(file, testWorkbook);

	assert.ok(subject.getWorkbook(file));
});

test('it can get a workbook by name, with extension, from the workbook map', (assert) => {
	let WorkbooksObject = EmberObject.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let file = 'test1';
	let ext = '.xlsx';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(file, testWorkbook);

	assert.ok(subject.getWorkbook(file + ext));
});

test('it clears the workbook map', (assert) => {
	let WorkbooksObject = EmberObject.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let file = 'test1';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(file, testWorkbook);
	subject.clear();

	assert.notOk(subject.get('_workbooks').size);
});

test('it can check, by name without extension, if a workbook is in the workbook map', (assert) => {
	let WorkbooksObject = EmberObject.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let file = 'test1';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(file, testWorkbook);

	assert.ok(subject.containsFile(file));
});

test('it can check, by name with extension, if a workbook is in the workbook map', (assert) => {
	let WorkbooksObject = EmberObject.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let file = 'test1';
	let ext = '.xlsx';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(file, testWorkbook);

	assert.ok(subject.containsFile(file + ext));
});



