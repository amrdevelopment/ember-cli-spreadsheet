import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('ember-spreadsheet', 'Integration | Component | ember spreadsheet', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{ember-spreadsheet}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#ember-spreadsheet}}
      template block text
    {{/ember-spreadsheet}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
